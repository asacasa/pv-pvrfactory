#!/bin/bash

set -x

src=$1
base_json=$src/.pvr/json
staging=$2
datadir="$staging/trails/0/"
metadir="$staging/trails/0/.pv/"
state_json=$datadir/.pvr/json
config=$datadir/.pvr/config
username=$3
password=$4

if test -z "$PANTAHUB_HOST"; then
	PANTAHUB_HOST=api.pantahub.com
fi
if test -z "$PANTAHUB_PORT"; then
	PANTAHUB_PORT=443
fi
if [ -z "$INITRD_IMG" ]; then
	INITRD_IMG="pantavisor.cpio.xz4"
fi
if [ -z "$KERNEL_IMG" ]; then
	KERNEL_IMG="kernel.img"
fi
if [ -z "$MODULES_IMG" ]; then
	MODULES_IMG="modules.squashfs"
fi
if [ -z "$FIRMWARE_IMG" ]; then
	FIRMWARE_IMG="firmware.squashfs"
fi

merge_state()
{
	cur=$pvjson
	tomerge="$1"

	pvjson=`echo $cur'
	'$tomerge | json --deep-merge`
}

merge_volume()
{
	vols=`echo $pvjson | json -0 volumes`
	tomerge="$1"
	name=`echo $tomerge | json -a`
	present=
	if [ ! -z "$vols" ]; then
		present=`echo $vols | json -a -c 'this == "'$name'"'`
	fi
	if [ -z "$present" ]; then
		vols=`echo $vols'
		'$tomerge | json --group -0`

		merge_state '{ "volumes": '$vols' }'
	fi
}

if [ -n "$PVR_MERGE_SRC" ]; then
       if ! echo $PVR_MERGE_SRC | grep -q -e ^/ -e https://; then
               PVR_MERGE_SRC=${TOP_DIR}"/"${PVR_MERGE_SRC}
       fi
fi


cd $src
rm -rf $src/pantavisor.json

# temp mv of build assets to clear staging
mkdir -p $src/.tmp/
if [ -e $src/.pvr ]; then
	mv $KERNEL_IMG $INITRD_IMG $MODULES_IMG $FIRMWARE_IMG $src/.tmp/ &> /dev/null
	rm -rf $src/.pvr &> /dev/null
	rm -f $src/* &> /dev/null
fi

$PVR init
$PVR commit

# merge with outer if provided
if [ ! -z "$PVR_MERGE_SRC" ]; then
	$PVR merge $PVR_MERGE_SRC
	$PVR checkout
fi

if [ -e $src/.tmp ]; then
	mv $src/.tmp/* $src/ &> /dev/null
fi
$PVR add
$PVR commit

pvjson=`cat $src/pantavisor.json 2>/dev/null`
# merge build assets
[ -e "$INITRD_IMG" ] && merge_state '{ "initrd": "'$INITRD_IMG'" }'
[ -e "$KERNEL_IMG" ] && merge_state '{ "linux": "'$KERNEL_IMG'" }'
[ -e "$MODULES_IMG" ] && merge_volume '["'$MODULES_IMG'"]'
[ -e "$FIRMWARE_IMG" ] && merge_volume '["'$FIRMWARE_IMG'"]'

echo $pvjson > $src/pantavisor.json
$PVR add
$PVR commit

mkdir -p $staging
$PVR put $staging

# continue in staging
cd $staging
mkdir -p $metadir
mkdir -p $datadir

# copy state json to final location
mkdir -p `dirname $state_json`
cp json $state_json

# set config with objects directory for on ddisk
echo '{ "ObjectsDir": "../../objects" }' > $config

# link objects
for i in `cat json | json -M -c 'this.key.split(".").pop() != "json" && this.key != "#spec"' -d: -a key value`; do
	filename=$datadir/`echo $i | awk -F ":" '{print $1}'`
	object=`echo $i | awk -F ":" '{print $2}'`
	mkdir -p `dirname $filename`
	if echo $filename | grep -q \.bind$; then
		cp -f $staging/objects/$object $filename
	else
		ln $staging/objects/$object $filename
	fi
done

# write json files
for i in `cat json | json -M -c 'this.key.split(".").pop() == "json"' -a key`; do
	buf=`cat json | json -M -c 'this.key == "'"$i"'"' -a value`
	filename=$datadir/$i
	mkdir -p `dirname $filename`
	echo $buf > $filename
done

prefix=
spec=`cat json | json "#spec"`
if [ "$spec" = "pantavisor-multi-platform@1" ]; then
	bsp="pantavisor.json"
else
	mv $src/$INITRD_IMG $src/$KERNEL_IMG $datadir/bsp/
	$PVR add $datadir/bsp/
	$PVR commit
	bsp="bsp/run.json"
	prefix="bsp/"
fi

# link kernel and initrd
kernel=`cat json | json -M -c 'this.key == "'"$bsp"'"' -a value | json linux`
if ! test -z "$kernel"; then
	ln $datadir/$prefix$kernel $metadir/pv-kernel.img
fi
initrd=`cat json | json -M -c 'this.key == "'"$bsp"'"' -a value | json initrd`
if ! test -z "$initrd"; then
	ln $datadir/$prefix$initrd $metadir/pv-initrd.img
fi

# remove old objects from final
for i in `ls $staging/objects/`; do
	grep -q $i $staging/json
	if [ $? -ne 0 ]; then
		rm $staging/objects/$i
	fi
done

if ! test -z "$PV_BL_IS_PVK"; then
	: > $datadir/kernel.img
fi

echo "Factory state:"
cat $staging/json | json

rm $staging/json
