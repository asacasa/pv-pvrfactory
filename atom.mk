
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := trail
LOCAL_CATEGORY_PATH := system

TRAIL_SRC_DIR := $(LOCAL_PATH)

$(call load-config)

# Build
trail:
	@echo "Using pvr to create factory trail step"
	@rm -rf $(TARGET_OUT_TRAIL_FINAL)/objects
	@rm -rf $(TARGET_OUT_TRAIL_FINAL)/trails
	@rm -rf $(TARGET_OUT_TRAIL_FINAL)/vendor
	@rm -rf $(TARGET_OUT_TRAIL_FINAL)/boot
	@if [ -e $(TARGET_VENDOR_DIR)/firmware ]; then \
		rm $(TARGET_OUT_TRAIL_STAGING)/firmware.squashfs; \
		mksquashfs $(TARGET_VENDOR_DIR)/firmware $(TARGET_OUT_TRAIL_STAGING)/firmware.squashfs -b 1048576 -comp xz -Xdict-size 100%; \
	fi
	@echo "Setting TOP_DIR: $(TOP_DIR)"
	@TOP_DIR=$(TOP_DIR) fakeroot $(TRAIL_SRC_DIR)/pvrfactory $(TARGET_OUT_TRAIL_STAGING) $(TARGET_OUT_TRAIL_FINAL) user1 user1
	@mkdir -p $(TARGET_OUT_TRAIL_FINAL)/boot/
	@mkdir -p $(TARGET_OUT_TRAIL_FINAL)/vendor/
	@echo -ne "pv_rev=0\0" > $(TARGET_OUT_TRAIL_FINAL)/boot/uboot.txt
	@if test ! -z "$(TARGET_FDT_FILE)"; then echo -ne "pv_fdtfile=$(TARGET_FDT_FILE)\0" >> $(TARGET_OUT_TRAIL_FINAL)/boot/uboot.txt; fi
	@mkdir -p $(TARGET_OUT_TRAIL_FINAL)/config/
	@if [ ! -e $(TARGET_CONFIG_DIR)/pantahub.config ]; then echo "ERROR: No pantahub.config file in $(TARGET_CONFIG_DIR), must exist before building trail"; exit 1; fi
	@cp $(TARGET_CONFIG_DIR)/pantahub.config $(TARGET_OUT_TRAIL_FINAL)/config/pantahub.config

include $(BUILD_CUSTOM)
